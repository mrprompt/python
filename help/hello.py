print ('Hello World')

# Comment

name = "Derek" # Single/double quotes are always interchangeable
print (name)
name = 15

print ('5 - 2=', 5-2)
print ('5 * 2=', 5*2)
print ('5 / 2=', 5/2)
print ('5 % 2=', 5%2) # Modulus - returns the remainder of a division
print ('5 ** 2=', 5**2) # Exponent
print ('5 // 2=', 5//2) # Floor division - discarded remainder
